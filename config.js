const path = require('path')

const resultsPath = path.resolve('results');
module.exports = {
  baseUrl: 'http://www.autocom.co.il',
  levelIndicator: '»',
  siteMapPath: path.join(resultsPath, 'siteMap.json'),
  uploadableFilesPath: path.join(resultsPath, 'uploadableFiles.json'),
  rawHtmlPath: path.join(resultsPath, 'rawHtmls'),
  extractedHtmlDir: path.join(resultsPath, 'extractedHtml'),
  preprocessedHtmlDir: path.join(resultsPath, 'preProcessedHtmls'),
  preparedHtmlDir: path.join(resultsPath, 'preparedHtmlDir'),
  rawHtmlMetaPath: path.join(resultsPath, 'rawHtmlsMeta.json'),
  missingFilesPath: path.join(resultsPath, 'missingFiles.json'),
  verifiedUploadableFilesPath: path.join(resultsPath, 'verifiedUploadableFilesPath.json'),
  uploadedFilesPath: path.join(resultsPath, 'uploadedFiles.json'),
  preProcessedMetaFile: path.join(resultsPath, 'preProcessedMetaFile.json'),
  extractedMetaFile: path.join(resultsPath, 'extractedMetaFile.json'),
  preparedMetaFile: path.join(resultsPath, 'preparedMetaFile.json'),
  reqResLogsFile: path.join(resultsPath, 'reqResLogsFile.json'),
  newBaseUrl: 'http://wordpress-432404-1413073.cloudwaysapps.com',
  downloadExistingFiles: false,
}