const {fetchSiteMap} = require('./stages/siteMap')
const {scrapeLinks} = require('./stages/scrapeLinks')
const {simpleExtractPages} = require('./stages/simpleExtractPage')
const {preUploadProcess} = require('./stages/preUploadProcess')
const {uploadAttachments} = require('./stages/uploadAttachments')
const {postUploadProcess} = require('./stages/postUploadProcess')
const {uploadEverything, updateTaxonomy} = require('./stages/uploadPosts')

async function run() {
  try {
    await fetchSiteMap()
    await scrapeLinks()
    await simpleExtractPages()
    await preUploadProcess()
    await uploadAttachments()
    await postUploadProcess()
    await uploadEverything()
    await updateTaxonomy()
  } catch (err) {
    if (err.response) {
      console.error(err.message);
      console.error(err.response.data);
    } else {
      console.error(err);
    }
  }
}

run();