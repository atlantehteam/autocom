module.exports.delay = function (time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time)
  });
}

module.exports.xClass = (cls) => `[contains(concat(' ', @class, ' '), ' ${cls} ')]`
module.exports.hasTxt = (txt) => `[contains(text(), '${txt}')]`

module.exports.getProp = async (handle, prop) => await (await handle.getProperty(prop)).jsonValue();

module.exports.getPropFromXpath = async (page, xpath, prop) => {
  const [handle] = await page.$x(xpath);
  if (!handle) { return null; }

  const val = await module.exports.getProp(handle, prop);
  if (!val) { return null }

  return val.trim();
}

module.exports.getXpathContent = async (page, xpath) => {
  return module.exports.getPropFromXpath(page, xpath, 'textContent')
}

module.exports.tryGetContent = async (page, xPaths) => {
  for (let i = 0; i < xPaths.length; i++) {
    const xpath = xPaths[i];
    const content = await module.exports.getXpathContent(page, xpath);
    if (content !== null) {
      return content;
    }
  }
  return null;
}