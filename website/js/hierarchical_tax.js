(function($) {
  function makeAjaxRequest(data) {
    return new Promise((resolve, reject) => {
      $.post(window.AutoCom.ajax_url, data)
        .done(resolve)
        .fail(err => {
          console.error(err);
          reject(err);
        })
    })
  }
  
  const cache = {};

  function getChildren(parent) {
    if (cache[parent]) {
      return Promise.resolve(cache[parent]);
    }
    return makeAjaxRequest({action: 'lpk_children', parent})
      .then(res => {
        cache[parent] = res;
        return res;
      })
  }
  function handleFilterChange() {
    $('.lpk-filter [data-step] select').change(function() {
      const $select = $(this)
      const $step = $(this).closest('[data-step]');
      const step = parseInt($step.attr('data-step'));
      const childSteps = $('.lpk-filter [data-step]').filter(function() {return $(this).attr('data-step') > step})
      const invalidOptions = childSteps.find('select option').filter(function() {return $(this).val()})
      invalidOptions.remove();
      
      const value = $select.val();
      $('.lpk-filter .lpk-result').addClass('hide');
      const $submitBtn = $('.lpk-filter .lpk-submit');
      $submitBtn.prop('disabled' , true);
      if (!value) {return;}
      if (childSteps.length) {
        getChildren(value)
          .then(res => {
            const $childSelect = $(`.lpk-filter [data-step=${step + 1}] select`);
            const optsStr = res.map(opt => `<option value="${opt.id}">${opt.name}</option>`)
            $childSelect.append(optsStr);
          })
      } else {
        $submitBtn.prop('disabled' , false);
      }
    })

    $('.lpk-filter').submit(function(e) {
      e.preventDefault();
      const value = $(this).find('[data-step=5] select').val();
      makeAjaxRequest({action: 'lpk_get_results', id: value})
        .then(res => {
          console.log(res);
          $('.lpk-filter .lpk-factory-wrap').text(res.factory_lpk)
          $('.lpk-filter .lpk-test-wrap').text(res.test_lpk)
          $('.lpk-filter .whpk-factory-wrap').text(res.factory_whpk)
          $('.lpk-filter .whpk-test-wrap').text(res.test_whpk)
          // $('.lpk-filter .kwh-factory-wrap').text(res.factory_km_per_kwh)
          $('.lpk-filter .kwh-test-wrap').text(res.test_km_per_kwh)
          $('.lpk-filter .lpk-result').removeClass('hide');
          $('.lpk-filter .lpk-result').toggleClass('is-electric', !!res.is_electric);
        })
    })
  }
  
  $(() => {
    handleFilterChange()
  })
})(window.jQuery);