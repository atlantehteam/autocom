<?php

require_once get_stylesheet_directory() . '/consumption-filter.php';
require_once get_stylesheet_directory() . '/consumption-table.php';
// require_once get_stylesheet_directory() . '/functions-import.php';

function hello_elementor_child_enqueue_scripts()
{
	wp_enqueue_style(
		'hello-elementor-child',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor'
		],
		filemtime(get_stylesheet_directory() . '/style.css')
	);
}
add_action('wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts');

// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);
