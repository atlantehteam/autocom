<?php


// Migrate posts
function uploadRemote($image_url)
{
  $content = file_get_contents($image_url);
  $wpFileType = wp_check_filetype($image_url, null);
  $type = $wpFileType['type'];
  if (!$type)
    return false;

  $mirror = wp_upload_bits(basename($image_url), '', $content);
  $attachment = array(
    'post_title' => basename($image_url),
    'post_mime_type' => $type
  );

  $attach_id = wp_insert_attachment($attachment, $mirror['file'], null, true);

  if ($type !== 'application/pdf') {
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata($attach_id, $mirror['file']);
    wp_update_attachment_metadata($attach_id, $attach_data);
  }
  update_post_meta($attach_id, 'original_url', $image_url);
  return array('id' => $attach_id, 'meta' => $attach_data, 'url' => $mirror['url'], 'isNew' => true);
}
function uploadRemoteImageAndAttach($image_url)
{
  $queryArgs = array(
    'post_type'   => 'attachment',
    'post_status' => 'inherit',
    'numberposts' => 1,
    'meta_query'  => array(
      array(
        'key'     => 'original_url',
        'value'   => $image_url
      )
    )
  );
  $attachments = get_posts($queryArgs);
  if (sizeof($attachments) > 0) {
    $attach_id = $attachments[0]->ID;

    $fileSize = filesize(get_attached_file($attach_id));
    if ($fileSize > 0) {
      return array('id' => $attach_id, 'fileSize' => $fileSize, 'url' => wp_get_attachment_url($attach_id), 'isNew' => false);
    }
    wp_delete_attachment($attach_id, true);
  }

  for ($i = 0; $i < 2; $i++) {
    $result = uploadRemote($image_url);
    $attach_id = $result['id'];
    $fileSize = filesize(get_attached_file($attach_id));
    if ($fileSize > 0) {
      return array('id' => $attach_id, 'fileSize' => $fileSize, 'url' => wp_get_attachment_url($attach_id), 'isNew' => false);
    }
    wp_delete_attachment($attach_id, true);
  }
  return array('err' => 'invalid file');
}

add_action('wp_ajax_nopriv_upload_file', 'upload_file_function');
add_action('wp_ajax_upload_file', 'upload_file_function');

function upload_file_function()
{
  elevate_script_permissions();
  $url = $_POST['file_url'];
  $result = uploadRemoteImageAndAttach($url);
  wp_send_json($result);
}

add_action('wp_ajax_nopriv_create_post', 'createPost');
add_action('wp_ajax_create_post', 'createPost');

function createPost()
{
  elevate_script_permissions();
  validateUploadDependencies();
  $html = base64_decode($_POST['html']);
  $title = $_POST['title'] ?? '';
  $imageId = $_POST['imageId'] ?? '';
  $categoryIds = $_POST['categoryIds'] ?? '';
  $description = $_POST['description'] ?? '';
  $originalUrl = $_POST['originalUrl'] ?? '';
  $existingPostId = 0;
  if (empty($originalUrl)) {
    wp_send_json_error(array('err' => 'missing original url'), 400);
  }

  $categoryIds = explode(',', $categoryIds);
  $args = array(
    'post_type'    => 'post',
    'meta_key'     => 'original_url',
    'meta_value'   => $originalUrl
  );

  $current = get_posts($args);
  if (sizeof($current)) {
    $existingPostId = $current[0]->ID;
  }
  $meta = [];
  if (!empty($description)) {
    $meta['_yoast_wpseo_metadesc'] = $description;
  }
  $time = current_time('mysql');
  $my_post = array(
    'ID'                        => $existingPostId,
    'post_title'    => $title,
    'post_content'  => $html,
    'post_date'     => $time,
    'post_date_gmt' => get_gmt_from_date($time),
    'post_status'   => 'publish',
    'post_category' => $categoryIds,
    'meta_input'        => $meta
  );
  $postId = wp_insert_post($my_post);
  set_post_thumbnail($postId, $imageId);
  $newPostUrl = get_the_permalink($postId);
  if ($postId != $existingPostId) {
    $originalPath = parse_url($originalUrl, PHP_URL_PATH);
    $redirectResults = Red_Item::create(array(
      'status' => 'enabled',
      "url" =>    $originalPath,
      "match_url" =>  $originalPath,
      'action_code' => 301,
      'action_type' => 'url',
      'action_data' => array('url' => $newPostUrl),
      'group_id' => 1,
      'match_type' => 'url',
    ));
  }

  update_post_meta($postId, 'original_url', $originalUrl);
  wp_send_json(array('id' => $postId, 'title' => $title, 'image' => $imageId, 'url' => $newPostUrl));
}

add_action('wp_ajax_nopriv_create_category', 'createCategory');
add_action('wp_ajax_create_category', 'createCategory');

function createCategory()
{
  elevate_script_permissions();
  validateUploadDependencies();
  $html = base64_decode($_POST['html']);
  $title = $_POST['title'];
  $parent = $_POST['parent'] ?? 0;
  $originalUrl = $_POST['originalUrl'] ?? '';

  if (empty($originalUrl)) {
    wp_send_json_error(array('err' => 'missing original url'), 400);
  }


  $existingPostId = get_cat_ID($title);
  $my_cat = array(
    'cat_ID' => $existingPostId,
    'cat_name'    => $title,
    'category_parent'  => $parent,
    'category_description' => $html
  );
  $catId = wp_insert_category($my_cat);

  if (!$existingPostId) {
    $newCatUrl = get_category_link($catId);
    $originalPath = parse_url($originalUrl, PHP_URL_PATH);
    $redirectResults = Red_Item::create(array(
      'status' => 'enabled',
      "url" =>    $originalPath,
      "match_url" =>  $originalPath,
      'action_code' => 301,
      'action_type' => 'url',
      'action_data' => array('url' => $newCatUrl),
      'group_id' => 1,
      'match_type' => 'url',
    ));
  }

  wp_send_json(array('id' => $catId, 'title' => $title, 'url' => get_category_link($catId)));
}

add_action('wp_ajax_nopriv_update_post_taxonomies', 'updateTaxonomies');
add_action('wp_ajax_update_post_taxonomies', 'updateTaxonomies');

function updateTaxonomies()
{
  elevate_script_permissions();
  validateUploadDependencies();
  $tax = $_POST['taxonomy'] ?? '';
  $terms = $_POST['terms'] ?? [];
  $originalUrl = $_POST['originalUrl'] ?? '';

  if (empty($tax)) {
    wp_send_json_error(array('err' => 'missing taxonomy name'), 400);
  }

  if (empty($originalUrl)) {
    wp_send_json_error(array('err' => 'missing original url'), 400);
  }

  $args = array(
    'post_type'    => 'post',
    'meta_key'     => 'original_url',
    'meta_value'   => $originalUrl
  );

  $current = get_posts($args);
  if (!sizeof($current)) {
    wp_send_json_error(array('err' => 'invalid original url'), 400);
  }
  $existingPostId = $current[0]->ID;

  $parentTerm = 0;
  $postTerms = [];
  foreach ($terms as $term) {
    $dbTerm = get_term_by('name', $term, $tax);
    $args = array(
      'name'    => $term,
      'parent'  => $parentTerm,
    );
    if ($dbTerm) {
      $termId = wp_update_term($dbTerm->term_id, $tax, $args)['term_id'];
    } else {
      $termId = wp_insert_term($term, $tax, $args)['term_id'];
    }
    $parentTerm = $termId;
    $postTerms[] = $termId;
  }
  wp_set_post_terms($existingPostId, $postTerms, $tax);

  //   $existingPostId = $term ? $term->term_id : 0;


  wp_send_json(array('id' => $termId, 'title' => $title));
}

function validateUploadDependencies()
{
  if (!is_plugin_active('wordpress-seo/wp-seo.php')) {
    wp_send_json_error(array('err' => 'Yoast plugin is not active\\installed'), 500);
  }
  if (!is_plugin_active('redirection/redirection.php')) {
    wp_send_json_error(array('err' => 'Redirection plugin is not active\\installed'), 500);
  }
  $status = new Red_Database_Status();
  if ($status->needs_installing()) {
    wp_send_json_error(array('err' => 'Redirection plugin needs to be setup'), 500);
  }
  if ($status->needs_updating()) {
    wp_send_json_error(array('err' => 'Redirection plugin needs db update'), 500);
  }
}

function elevate_script_permissions()
{
  wp_set_current_user(null, 'roi');
}

function insert_consumption_term($name, $parent)
{
  $res = wp_insert_term($name, 'consumption-hierarchy', array('parent' => $parent));
  $term = is_wp_error($res) ? $res->error_data['term_exists'] : $res['term_id'];
  if (!$term) {
    wp_send_json("$name & $parent resulted invalid term: $term", 500);
  }
  return $term;
}

add_action('wp_ajax_nopriv_import_cars', 'roi_import_cars');
add_action('wp_ajax_import_cars', 'roi_import_cars');
function roi_import_cars()
{
  $file = file_get_contents(__DIR__ . '/cars.json');
  $items = json_decode($file, TRUE);
  foreach ($items as $item) {
    $parent = insert_consumption_term($item['company'], 0);
    $parent = insert_consumption_term($item['brand'], $parent);
    $parent = insert_consumption_term($item['year'], $parent);
    $parent = insert_consumption_term('בנזין', $parent);
    $parent = insert_consumption_term($item['volume'], $parent);
    update_term_meta($parent, 'factory_lpk', $item['factory']);
    update_term_meta($parent, 'test_lpk', $item['test']);
  }
  wp_send_json(array('success' => true));
}

// Migrate posts - END
