<?php

function get_volume_consumption($volume)
{
  $meta = get_term_meta($volume->term_id);
  $factory_lpk = $meta['factory_lpk'][0] ?? '?';
  $test_lpk = $meta['test_lpk'][0] ?? '?';
  $engine_type = $meta['engine_type'][0] ?? '?';
  if ($engine_type == 'electric') {
    $factory_whpk = $meta['factory_whpk'][0] ?? '?';
    $test_whpk = $meta['test_whpk'][0] ?? '?';
    $factory = "<div>טווח נסיעה: {$factory_lpk} ק\"מ</div><div>צריכת חשמל: {$factory_whpk} וואט-שעה לק\"מ</div>";
    $test = "<div>טווח נסיעה: {$test_lpk} ק\"מ</div><div>צריכת חשמל: {$test_whpk} וואט-שעה לק\"מ</div>";
  } else {
    $factory = "<div>{$factory_lpk} ק\"מ לליטר</div>";
    $test = "<div>{$test_lpk} ק\"מ לליטר</div>";
  }
  return ['factory' => $factory, 'test' => $test];
}

add_shortcode('ac_consumption_table', 'generate_consumption_table');

function generate_consumption_table()
{
  $terms = get_terms(array('taxonomy' => 'consumption-hierarchy', 'hide_empty' => false));
  $models = array_filter($terms, function ($itm) {
    return empty($itm->parent);
  });

  $parent_map = array();
  foreach ($terms as $element) {
    $parent_map[$element->parent][] = $element;
  }

  $min_year = intval(date('Y')) - 3;
  $rows_str = '';
  foreach ($models as $model) {
    $brands = $parent_map[$model->term_id];
    foreach ($brands as $brand) {
      $raw_years = $parent_map[$brand->term_id] ?? [];
      $years = array_filter($raw_years, function ($itm) use ($min_year) {
        return intval($itm->name) >= $min_year;
      });
      foreach ($years as $year) {
        $types = $parent_map[$year->term_id] ?? [];
        foreach ($types as $type) {
          $volumes = $parent_map[$type->term_id] ?? [];
          foreach ($volumes as $volume) {
            $consumption = get_volume_consumption($volume);
            $rows_str .= "<tr><td>{$model->name}</td><td>{$brand->name}</td><td>{$year->name}</td><td>{$type->name}</td><td>{$volume->name}</td><td class='fuel-val'>{$consumption['factory']}</td><td class='fuel-val'>{$consumption['test']}</td></tr>";
          }
        }
      }
    }
  }
  return "<div class='fuel-tbl-wrap'><table class='fuel-tbl'><thead>" .
    "<tr><th>יצרן</th><th>דגם</th><th>שנת יצור</th><th>סוג מנוע</th><th>נפח מנוע</th><th class='fuel-val'>צריכה - נתוני יצרן</th><th class='fuel-val'>צריכה - מבחן כביש</th></tr>" .
    "</thead><tbody>$rows_str</tbody></table></div>";
}
