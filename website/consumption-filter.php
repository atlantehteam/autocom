<?php

function getEngineTerms($parent)
{
  $terms = get_terms(array(
    'taxonomy' => 'consumption-hierarchy',
    'hide_empty' => false,
    'parent' => $parent,
  ));
  return $terms;
}
function generate_consumption_filter()
{
  $tax = 'consumption-hierarchy';
  $terms = getEngineTerms(0);

  echo '<form class="lpk-filter" id="lpk-filter" method="post" action="">';
  echo '<div class="lpk-fields-wrapper">';
  echo '<div class="lpk-step select-wrapper" data-step="1">';
  echo '<select class="parent_tax">' . "\n";
  echo "<option value=''>" . __('יצרן', 'hello-elementor-child') . "</option>";
  $childs_term = array();
  foreach ($terms as $term) {
    echo "<option value='{$term->term_id}'>{$term->name}</option>\n";
  }
  echo "</select>\n";
  echo "</div>";

  echo '<div class="lpk-step select-wrapper" data-step="2">';
  echo "<select class='sub_tax'>";
  echo "<option value=''>" . __('דגם', 'hello-elementor-child') . "</option>";
  echo "</select>";
  echo "</div>";

  echo '<div class="lpk-step select-wrapper" data-step="3">';
  echo "<select class='sub_tax'>";
  echo "<option value=''>" . __('שנת יצור', 'hello-elementor-child') . "</option>";
  echo "</select>";
  echo "</div>";

  echo '<div class="lpk-step select-wrapper" data-step="4">';
  echo "<select class='sub_tax'>";
  echo "<option value=''>" . __('סוג מנוע', 'hello-elementor-child') . "</option>";
  echo "</select>";
  echo "</div>";

  echo '<div class="lpk-step select-wrapper" data-step="5">';
  echo "<select class='sub_tax'>";
  echo "<option value=''>" . __('נפח מנוע', 'hello-elementor-child') . "</option>";
  echo "</select>";
  echo "</div>";

  echo '<div class="lpk-submit-wrapper">';
  echo "<button class='lpk-submit' type='submit' disabled>" . __('חיפוש', 'hello-elementor-child') . "</button>";
  echo "</div>";
  echo '</div>';
  echo '<div class="lpk-result hide">';
  echo '<div class="lpk-row">';
  echo '<div class="lpk-factory-wrap lpk-item"></div>';
  echo '<div class="lpk-test-wrap test-wrap lpk-item"></div>';
  echo '</div>';
  echo '<div class="lpk-row eletric-only">';
  echo '<div class="whpk-factory-wrap lpk-item"></div>';
  echo '<div class="whpk-test-wrap test-wrap lpk-item"></div>';
  echo '</div>';
  echo '<div class="lpk-row eletric-only">';
  echo '<div class="kwh-factory-wrap lpk-item"></div>';
  echo '<div class="kwh-test-wrap test-wrap lpk-item"></div>';
  echo '</div>';
  echo "</div>";
  echo "</form>";
}

function consumption_filter_scripts()
{
  $theme = wp_get_theme('hello-theme-child-master');
  global $post;
  if (is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'consumption_filter')) {
    wp_enqueue_style('hierarchical_tax_css', get_stylesheet_directory_uri() . '/css/hierarchical_tax.css', array(), $theme['Version']);
    wp_enqueue_script('hierarchical_tax_js', get_stylesheet_directory_uri() . '/js/hierarchical_tax.js', array('jquery'), $theme['Version'], true);
    wp_localize_script('hierarchical_tax_js', 'AutoCom', array('ajax_url' => admin_url('admin-ajax.php')));
  }
}
add_action('wp_footer', 'consumption_filter_scripts');

add_shortcode('consumption_filter', 'generate_consumption_filter');

add_action('wp_ajax_lpk_children', 'lpk_get_children');
add_action('wp_ajax_nopriv_lpk_children', 'lpk_get_children');

function lpk_get_children()
{
  if (!isset($_POST['parent'])) {
    wp_send_json_error('', 400);
  }
  $terms = getEngineTerms($_POST['parent']);
  $results = array();
  foreach ($terms as $term) {
    $results[] = array('id' => $term->term_id, 'name' => $term->name);
  }
  wp_send_json($results);
}

add_action('wp_ajax_lpk_get_results', 'lpk_get_results');
add_action('wp_ajax_nopriv_lpk_get_results', 'lpk_get_results');

function lpk_get_results()
{
  if (!isset($_POST['id'])) {
    wp_send_json_error('', 400);
  }
  $factory_lpk = get_term_meta($_POST['id'], 'factory_lpk', true) ?: '?';
  $test_lpk = get_term_meta($_POST['id'], 'test_lpk', true) ?: '?';
  $engine_type = get_term_meta($_POST['id'], 'engine_type', true);
  if ($engine_type == 'electric') {
    $factory_whpk = get_term_meta($_POST['id'], 'factory_whpk', true) ?: '?';
    $test_whpk = get_term_meta($_POST['id'], 'test_whpk', true) ?: '?';
    $factory_km_per_kwh = get_term_meta($_POST['id'], 'factory_km_per_kwh', true) ?: '?';
    $test_km_per_kwh = get_term_meta($_POST['id'], 'test_km_per_kwh', true) ?: '?';
    $results = array(
      'factory_lpk' => sprintf(__('נתוני יצרן: טווח נסיעה - %s ק"מ', 'hello-elementor-child'), $factory_lpk),
      'test_lpk' => sprintf(__('מבחן כביש: טווח נסיעה - %s ק"מ', 'hello-elementor-child'), $test_lpk),
      'factory_whpk' => sprintf(__('נתוני יצרן: צריכת חשמל - %s וואט-שעה לק"מ', 'hello-elementor-child'), $factory_whpk),
      'test_whpk' => sprintf(__('מבחן כביש: צריכת חשמל - %s וואט-שעה לק"מ', 'hello-elementor-child'), $test_whpk),
      'factory_km_per_kwh' => sprintf(__('נתוני יצרן: טווח נסיעה ל-1 קוט"ש - %s ק"מ', 'hello-elementor-child'), $factory_km_per_kwh),
      'test_km_per_kwh' => sprintf(__('מבחן כביש: טווח נסיעה ל-1 קוט"ש - %s ק"מ', 'hello-elementor-child'), $test_km_per_kwh),
      'is_electric' => true,
    );
  } else {
    $results = array(
      'factory_lpk' =>  sprintf(__('נתוני יצרן: %s ק"מ לליטר', 'hello-elementor-child'), $factory_lpk),
      'test_lpk' => sprintf(__('מבחן כביש: %s ק"מ לליטר', 'hello-elementor-child'), $test_lpk),
      'factory_whpk' =>  '',
      'test_whpk' => '',
      'factory_km_per_kwh' => '',
      'test_km_per_kwh' => '',
    );
  }


  wp_send_json($results);
}

add_action('admin_menu', 'shmeh_menu');
function shmeh_menu()
{
  add_menu_page('צריכת דלק', 'צריכת דלק', 'manage_options', 'edit-tags.php?taxonomy=consumption-hierarchy&post_type=fuel-consumption', '', 'dashicons-filter', 5);
}
