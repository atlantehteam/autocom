const chalk = require('chalk');
const readline = require('readline');

module.exports.logStep = function(step) {
  console.log(chalk.green(step));
}

module.exports.logError = function(error) {
  console.error(chalk.red(error));
}

module.exports.logProgress = function(str) {
  readline.clearLine(process.stdout);
  readline.cursorTo(process.stdout, 0);
  process.stdout.write(str);
}