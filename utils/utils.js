const axios = require('axios');
const cheerio = require('cheerio')

module.exports.loadSiteText = async url => {
  const {data} = await axios.get(url, {headers: {
    Accept: 'text/html',
    'Accept-Encoding': 'gzip',
  }});
  return data;
}

module.exports.toDOM = (str) => {
  return cheerio.load(str, {decodeEntities: false});
}


module.exports.toBase64Safe = url => {
  return Buffer.from(url).toString('base64').replace(/\//g, '_')
}

module.exports.fromBase64Safe = base64 => {
  const unsafeBase64 = base64.replace(/_/g, '/');
  return Buffer.from(unsafeBase64, 'base64').toString('utf8')
}

module.exports.chunk = (array, chunkSize) => {
  if (!chunkSize || chunkSize < 0) {
    throw new Error(`Invalid chunk size ${chunkSize}`)
  }
  const result = [];
  for (let i = 0; i < array.length; i += chunkSize) {
    result.push(array.slice(i, i + chunkSize));
    // do whatever
  }
  return result;
}

module.exports.safeEncodeURI = function(uri) {
  let decodedUri = uri
  try {
    decodedUri = decodeURI(uri);
  } catch (err) {
    return uri;
  }
  return encodeURI(decodedUri);
}