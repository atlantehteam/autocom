const fs = require('fs-extra')
const path = require('path');
const config = require('../config');
const {loadSiteText, toBase64Safe} = require('../utils/utils');
const { logStep, logProgress } = require('../utils/consoleUtils');

let counter = 0;
const cache = {};
const multiples = []


const processLink = async (item, warnings) => {
  const {title, link} = item;
  if (cache[item.base64]) {
    multiples.push(item.base64)
  } else {
    cache[item.base64] = true;
  }
  try {
    logProgress(`processing item ${++counter}: ${title}`);
    const safeBase64 = toBase64Safe(link);
    const rawHtmlPath = path.join(config.rawHtmlPath, `${safeBase64}.html`);
    const fileExists = await fs.exists(rawHtmlPath);
    if (!fileExists || config.downloadExistingFiles) {
      const text = await loadSiteText(encodeURI(link));
      await fs.outputFile(rawHtmlPath, text);
    }
    item.rawHtml = rawHtmlPath
    item.base64 = safeBase64
  } catch (err) {
    item.err = err.message;
    warnings.push(`${title} failed with ${err.message}`);
  }

  if (item.children) {
    for (let i = 0; i < item.children.length; i++) {
      await processLink(item.children[i], warnings)
    }
  }
}
module.exports.scrapeLinks = async function() {
  logStep('Scraping links...')
  const itemsStr = await fs.readFile(config.siteMapPath, 'utf8');
  const items = JSON.parse(itemsStr)
  const warnings = [];
  const promises = items.map(item => processLink(item, warnings));
  await Promise.all(promises);
  const results = {
    multiples,
    warnings,
    items,
  };
  console.log();
  await fs.outputFile(config.rawHtmlMetaPath, JSON.stringify(results, null, 2));
  logStep('DONE - Scraping Links\n')

}
