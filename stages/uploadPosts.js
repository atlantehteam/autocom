const fs = require('fs-extra')
const config = require('../config');
const axios = require('axios');
const querystring = require('querystring');
const qs = require('qs');
const { logProgress } = require('../utils/consoleUtils');


async function insertCategory({link, children: _, preparedHtml, ...item}, parent, reqResLogs) {
  const rawHtml = await fs.readFile(preparedHtml, 'utf-8');
  const base64Html = Buffer.from(rawHtml).toString('base64')
  const payload = {...item, html: base64Html, originalUrl: link, parent};
  const stringified = querystring.stringify(payload)
  const {data} = await axios.post('wp-admin/admin-ajax.php?action=create_category', stringified, {baseURL: config.newBaseUrl})
  reqResLogs.push({req: payload, res: data})
  return data.id
}

async function insertPost({link, preparedHtml, ...item}, categoryIds, reqResLogs) {
  if (!preparedHtml) {
    return;
  }
  const rawHtml = await fs.readFile(preparedHtml, 'utf-8');
  const base64Html = Buffer.from(rawHtml).toString('base64')
  const payload = {...item, html: base64Html, originalUrl: link, categoryIds: categoryIds.join(',')};
  const stringified = querystring.stringify(payload)
  const {data} = await axios.post('wp-admin/admin-ajax.php?action=create_post', stringified, {baseURL: config.newBaseUrl})
  reqResLogs.push({req: payload, res: data})
  return data.id
}

let counter = 0;
const uploadItem = async(item, catIds, reqResLogs) => {
  logProgress(`processing ${++counter} - ${item.title}`);
  if (item.children) {
    const newCatId = await insertCategory(item, catIds, reqResLogs);
    for (let i = 0; i < item.children.length; i++) {
      const child = item.children[i];
      await uploadItem(child, [newCatId, ...catIds], reqResLogs);
    }
  } else {
    await insertPost(item, catIds, reqResLogs);
  }
}
module.exports.uploadEverything = async function() {
  const reqResLogs = [];
  const preStr = await fs.readFile(config.preparedMetaFile, 'utf-8');
  const meta = JSON.parse(preStr);

  for (let i = 0; i < meta.items.length; i++) {
    const item = meta.items[i];
    await uploadItem(item, [], reqResLogs); 
  }
  console.log(); // new line
  await fs.outputFile(config.reqResLogsFile, JSON.stringify(reqResLogs, null, 2));
  console.log('Done!')
}

function findItemBy(array, cb) {
  if (!array) {return false;}
  const item = array.find(a => cb(a));
  if (item) {return item;}
  return array.find(a => findItemBy(a.children, cb));
}

async function updateItem(item) {
  const payload = {...item, taxonomy: 'brands'};
  const stringified = qs.stringify(payload)
  try {
    const {data, status} = await axios.post('wp-admin/admin-ajax.php?action=update_post_taxonomies', stringified, {baseURL: config.newBaseUrl})
  } catch (err) {
    console.log(`error: ${err.message} on post: ${item.postTitle}`);
  }
}

module.exports.updateTaxonomy = async function() {
  const preStr = await fs.readFile(config.preparedMetaFile, 'utf-8');
  const meta = JSON.parse(preStr);

  const cars = findItemBy(meta.items, i => i.title === 'סקירת מכוניות ומבחנים');

  const posts = cars.children.reduce((aggr, c) => [...aggr, ...(c.children || [])], []);
  const mapped = posts.map(p => ({postTitle: p.title, originalUrl: p.link, terms: p.title.split(/ +/).slice(0, 2)}))
  console.log(posts.filter(p => !p.link).length)
  let counter = 0;
  for (let i = 0; i < mapped.length; i++) {
    const item = mapped[i];
    await updateItem(item);
    logProgress(`processing ${++counter}/${mapped.length} - ${item.postTitle}`);
  }
  // console.log(); // new line
  console.log('Done!')
}