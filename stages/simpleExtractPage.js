const fs = require('fs-extra')
const path = require('path');
const cheerio = require('cheerio');
const PromisePool = require('@supercharge/promise-pool')
const config = require('../config');
const {toDOM, toBase64Safe} = require('../utils/utils');
const { logStep, logProgress } = require('../utils/consoleUtils');
const maxNameLength = 260 - config.rawHtmlPath.length - 6;
let counter = 0;

function extractContent($dom) {
  const $page = $dom('#printPage');
  let $content
  if ($page.length > 1) {
    throw new Error(`Invalid number of page elements ${$page.length}`);
  } else if ($page.length) {
    $content = toDOM(cheerio.html($page[0]))
  } else {
    const $iframe = $dom('.news .body iframe');
    if ($iframe.length !== 1) {
      throw new Error(`Invalid number of iframe elements ${$iframe.length}`);
    } 
    $content = toDOM(cheerio.html($iframe[0]));
  }
  return $content;
}


function addDescription($dom, item) {
  const metaDesc = $dom('meta[name=description]');
  if (metaDesc.length) {
    item.description = $dom('meta[name=description]')[0].attribs.content;
  }
}

function clearInvalidText(str) {
  return str.replace(/<xml>.*<\/xml>/gm, '').replace(/&#xA/g, ' ').replace(/style="[^"]*"/gm, '');
}

const preUploadWork = async (item, warnings) => {
  const {title, link, rawHtml} = item;

  try {
    logProgress(`Simple Extracting item ${++counter}: ${title}`);

    if (rawHtml) {
      const fileStr = await fs.readFile(rawHtml, 'utf-8');
      const domStr = clearInvalidText(fileStr);
      let $dom = toDOM(domStr);
      addDescription($dom, item);
      
      $dom = extractContent($dom);

      const output = $dom('body').html();
      const safeBase64 = toBase64Safe(link);
      const extractedHtmlDir = path.join(config.extractedHtmlDir, `${safeBase64.slice(0, maxNameLength)}.html`);
      await fs.outputFile(extractedHtmlDir, output);
      item.extractedHtml = extractedHtmlDir
    }
  } catch (err) {
    item.err = err.message;
    warnings.push(`${title} failed with ${err.message}`);
  }
}

const generatePreUploadJobs = (items, warnings) => {
  let jobs = items.map(item => {
    const job = () => preUploadWork(item, warnings);
    return job;
  });
  items.forEach(item => {
    if (item.children) {
      const childJobs = generatePreUploadJobs(item.children, warnings)
      jobs = [...jobs, ...childJobs];
    }
  });
  return jobs;
}

module.exports.simpleExtractPages = async function() {
  logStep('Simple Extract Processing...')
  const str = await fs.readFile(config.rawHtmlMetaPath, 'utf-8');
  const {items} = JSON.parse(str);
  const warnings = [];
  const promises = generatePreUploadJobs(items, warnings);
  await PromisePool.for(promises)
    .withConcurrency(40)
    .process(job => job());

  console.log(); // new line
  const results = {warnings, items}
  await fs.outputFile(config.extractedMetaFile, JSON.stringify(results, null, 2));
  logStep('Done - Simple Extract Processing')

}

