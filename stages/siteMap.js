const fs = require('fs-extra')
const axios = require('axios');
const config = require('../config')
const {toDOM, toBase64Safe} = require('../utils/utils');

const processLevel = (levelString, depth = 1) => {
  const repeatedIndicator = config.levelIndicator.repeat(depth + 1);
  const separator = ` ${repeatedIndicator} `;
  const children = levelString.split(separator);
  const [current] = children.splice(0, 1);
  const $current = toDOM(current);
  const currentEl = $current('a');
  if (!currentEl) {
    throw new Error('el not found')
  }
  const title = currentEl.text().trim();
  const link = currentEl.attr('href');
  if (!title || !link) {
    throw new Error(`missing title: ${title}, link: ${link}`)
  }

  const result = {title, link, base64: toBase64Safe(link)};
  if (children.length) {
    result.children = children.map(child => processLevel(child, depth + 1));
  }
  return result;
}

module.exports.fetchSiteMap = async function() {
  const {data} = await axios.get(`${config.baseUrl}/SiteMap.aspx`, {headers: {
    Accept: 'text/html',
    'Accept-Encoding': 'gzip',
  }});
  const {levelIndicator} = config;
  const $ = toDOM(data);
  const body = $('.news .body');
  const bodyText = body.html();
  const topLevels = bodyText.split(` ${levelIndicator} `).slice(1);
  const results = topLevels.map(level => processLevel(level, 1));
  await fs.outputFile(config.siteMapPath, JSON.stringify(results, null, 2));
}
