const fs = require('fs-extra')
const path = require('path');
const cheerio = require('cheerio');
const PromisePool = require('@supercharge/promise-pool')
const config = require('../config');
const {toDOM, toBase64Safe} = require('../utils/utils');
const { logStep, logProgress } = require('../utils/consoleUtils');
const maxNameLength = 260 - config.rawHtmlPath.length - 6;
let counter = 0;

function clearInvalidContent(item, $dom, warnings) {
  const $scripts = $dom('script');
  $scripts.remove();
  const $styles = $dom('style');
  $styles.remove();
  const $title = $dom('.TitleCategoryText');
  if ($title.length > 1) {
    warnings.push(`${item.title} - Invalid number of title elements: ${$title.length}`)
  }
  $title.remove();

  $dom('*').each(function() {      // iterate over all elements
    delete this.attribs['style']
    delete this.attribs['dir']
  });

  $dom('img').each(function() {      // iterate over all elements
    delete this.attribs['width']
    delete this.attribs['height']
  });

  $dom('br').remove();

  const $footer = $dom('.bLine')
  const footerText = $footer.text().split('\t').map(i => i.trim()).filter(i => i).join(' ')
  if (footerText && footerText !== 'הדפסה שלח לחבר') {
    warnings.push(`${item.title} - Invalid footer found: ${footerText}`);
  }
  $footer.remove();

  const $comments = $dom.root()
    .find('*')
    .contents()
    .filter(function() {return this.type === 'comment'});
  $comments.remove();
}

function verifyContent(item, $body, warnings) {
  const $title = $body('.TitleCategoryText')
  if ($title.length !== 1) {
    warnings.push(`${item.title} - Invalid number of title elements found: ${$title.length}`);
  } else {
    const htmlTitle = $title.text().trim();
    if (htmlTitle !== item.title) {
      warnings.push(`${item.title} has Invalid title ${htmlTitle}`)
      item.docTitle = htmlTitle;
    }
  }
}

async function extractFeatureImage(item, $dom, warnings) {
  try {
    const featureImg = $dom('img').first();
    if (featureImg.length) {
      item.image = featureImg[0].attribs.src;
      featureImg.remove();
    }
  }
  catch (err) {
    warnings.push(`${item.title} has invalid image: ${err.message}`);
  }
}

function removeEmptyLines(str) {
  const irregularSpace = ' ';
  const lines = str.replace(new RegExp(irregularSpace, 'g'), ' ').replace(/<font size="3"> +<\/font>/g, '').replace(/> +</g, '><').split('\n');
  const trimmedLines = lines.filter(l => l.trim());
  const result = trimmedLines.join('\n');
  return result;
}

function changeAbsolutePath(str) {
  return str.replace(/href="https?:\/\/www\.autocom\.co\.il\//g, 'href="/');
}

const preUploadWork = async (item, warnings) => {
  const {title, link, extractedHtml} = item;

  try {
    logProgress(`Pre Upload Processing item ${++counter}: ${title}`);

    if (extractedHtml) {
      const fileStr = await fs.readFile(extractedHtml, 'utf-8');
      const domStr = changeAbsolutePath(fileStr);
      const $dom = toDOM(domStr);
      verifyContent(item, $dom, warnings);
      clearInvalidContent(item, $dom, warnings);
      extractFeatureImage(item, $dom, warnings);

      const rawOutput = $dom('body').html();
      const output = removeEmptyLines(rawOutput)
      const safeBase64 = toBase64Safe(link);
      const preprocessedHtmlDir = path.join(config.preprocessedHtmlDir, `${safeBase64.slice(0, maxNameLength)}.html`);
      await fs.outputFile(preprocessedHtmlDir, output);
      item.preProcessedHtml = preprocessedHtmlDir
    }
  } catch (err) {
    item.err = err.message;
    warnings.push(`${title} failed with ${err.message}`);
  }
}

const generatePreUploadJobs = (items, warnings) => {
  let jobs = items.map(item => {
    const job = () => preUploadWork(item, warnings);
    return job;
  });
  items.forEach(item => {
    if (item.children) {
      const childJobs = generatePreUploadJobs(item.children, warnings)
      jobs = [...jobs, ...childJobs];
    }
  });
  return jobs;
}


module.exports.preUploadProcess = async function() {
  logStep('Pre Upload Processing...')
  const str = await fs.readFile(config.extractedMetaFile, 'utf-8');
  const {items} = JSON.parse(str);
  const warnings = [];
  const promises = generatePreUploadJobs(items, warnings);
  await PromisePool.for(promises)
    .withConcurrency(40)
    .process(job => job());

  console.log(); // new line
  const results = {warnings, items}
  await fs.outputFile(config.preProcessedMetaFile, JSON.stringify(results, null, 2));
  logStep('DONE - Pre Upload Processing\n')
}

