const fs = require('fs-extra')
const config = require('../config');
const path = require('path');
const PromisePool = require('@supercharge/promise-pool')
const {toDOM, chunk, safeEncodeURI} = require('../utils/utils');
const {logStep, logError, logProgress} = require('../utils/consoleUtils');
const axios = require('axios');
const querystring = require('querystring')

const {preprocessedHtmlDir} = config;

let counter = 0;
const retrieveUploadableFromFile = async function(filePath, result) {
  logProgress(`Processing ${++counter}`)
  const str = await fs.readFile(path.join(preprocessedHtmlDir, filePath), 'utf8')
  const $dom = toDOM(str);
  const $src = $dom('[src]');
  const $href = $dom('[href]');
  const srcValues = Array.from($src.map((i, el) => el.attribs.src));
  const hrefValues = Array.from($href.map((i, el) => el.attribs.href));
  const values = [...srcValues, ...hrefValues];
  values
    .filter(v => /^\/[^/]/.test(v) || v.startsWith('http://www.autocom'))
    .filter(v => !v.includes('.aspx'))
    .forEach(val => {
      result.push(val);
    })
}

const collectFeatureImages = (items) => {
  if (!items) {return []}

  const images = items.filter(i => i.image).map(i => i.image);
  const childImages = items.reduce((aggr, itm) => {
    return [...aggr, ...collectFeatureImages(itm.children)];
  }, [])
  return [...images, ...childImages];
}
const collectUploadableFiles = async function() {
  logStep('Collect Uploadable Files')
  const metaFileStr = await fs.readFile(config.preProcessedMetaFile);
  const metaFile = JSON.parse(metaFileStr);
  const featureImages = collectFeatureImages(metaFile.items);
  const result = featureImages;
  const files = await fs.readdir(preprocessedHtmlDir);
  const promises = files.map(f => () => retrieveUploadableFromFile(f, result));
  await PromisePool.for(promises)
    .withConcurrency(40)
    .process(job => job());
  await Promise.all(promises);

  console.log(); // new line
  const unique = [...new Set(result)]
  const extensions = {};
  unique.forEach(u => extensions[path.extname(u)] = true);
  await fs.outputFile(config.uploadableFilesPath, JSON.stringify(unique, null, 2));
  console.log(extensions);
  console.log('DONE - Collect Uploadable Files');
}

async function verifyUrl(url, missingFiles) {
  try {
    await axios.head(safeEncodeURI(url), {baseURL: 'http://www.autocom.co.il/'})
  } catch (err) {
    if (!err.response) {
      logError(url, err)
    }
    missingFiles.push({url, err: err.message});
  }
}
const verifyExistingFiles = async function() {
  logStep('Verify Existing Files')
  const str = await fs.readFile(config.uploadableFilesPath);
  const files = JSON.parse(str);
  let counter = 0;
  const missingFiles = [];

  await PromisePool.for(files)
    .withConcurrency(20)
    .process(async file => {
      logProgress(`Verifying ${++counter} - ${file}`);
      await verifyUrl(file, missingFiles)
    });

  console.log(); // new line
  console.log(missingFiles);
  await fs.outputFile(config.missingFilesPath, JSON.stringify(missingFiles, null, 2));
}

const removeMissingFiles = async function() {
  logStep('Remove Missing Files')
  const uploadableStr = await fs.readFile(config.uploadableFilesPath);
  const missingFilesStr = await fs.readFile(config.missingFilesPath);
  const uploadableFiles = JSON.parse(uploadableStr);
  const missingObjFiles = JSON.parse(missingFilesStr);
  const missingFiles = missingObjFiles.map(m => m.url)
  const validFiles = uploadableFiles.filter(f => !missingFiles.includes(f))
  await fs.outputFile(config.verifiedUploadableFilesPath, JSON.stringify(validFiles, null, 2));
}

const chunkAndUpload = async function() {
  logStep('Upload files')
  const str = await fs.readFile(config.verifiedUploadableFilesPath);
  const filesToUpload = JSON.parse(str);
  const warnings = []
  const uploaded = {}
  let counter = 0;
  await PromisePool.for(filesToUpload)
    .withConcurrency(5)
    .process(async fileUrl => {
      logProgress(`Uploading ${++counter} - ${fileUrl}`);
      const absolutePath = (new URL(fileUrl, config.baseUrl)).toString()
      const encodedPath = safeEncodeURI(absolutePath);
      const payload = querystring.stringify({file_url: encodedPath})
      try {
        const {data} = await axios.post('wp-admin/admin-ajax.php?action=upload_file', payload, {baseURL: config.newBaseUrl})
        uploaded[fileUrl] = data;
      } catch (err) {
        warnings.push(`${fileUrl} (${encodedPath}) failed to upload: ${err.message}`)
        console.log(`${absolutePath} failed to upload: ${err.message}`);
      }
      if (counter % 10 === 0) {
        await fs.outputFile(config.uploadedFilesPath, JSON.stringify({warnings, items: uploaded}, null, 2));
      }
    });

  console.log(); // new line
  await fs.outputFile(config.uploadedFilesPath, JSON.stringify({warnings, items: uploaded}, null, 2));
  logStep('DONE - Upload Attachments');
}

module.exports.uploadAttachments = async function() {
  await collectUploadableFiles();
  await verifyExistingFiles();
  await removeMissingFiles();
  await chunkAndUpload();
}