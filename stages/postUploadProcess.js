const fs = require('fs-extra')
const path = require('path')
const PromisePool = require('@supercharge/promise-pool')
const config = require('../config');
const {toDOM} = require('../utils/utils');
const { logStep, logProgress } = require('../utils/consoleUtils');

function replaceUploadedFiles($dom, uploadedMap) {
  const $imgs = $dom('[src]');
  $imgs.attr('src', function(i, src) {
    const uploaded = uploadedMap[src]
    if (uploaded) {
      return uploaded.url;
    }
    return src;
  })
  const $anchors = $dom('[href]');
  $anchors.attr('href', function(i, href) {
    const uploaded = uploadedMap[href]
    if (uploaded) {
      return uploaded.url;
    }
    return href;
  })
}

function updateFeatureImage(item, uploadedMap) {
  if (item.image) {
    const uploaded = uploadedMap[item.image];
    if (uploaded) {
      item.imageId = uploaded.id
    } else {
      console.log(`item ${item.title} has invalid image`);
    }
  }
}

let counter = 0;
async function postUploadJob(item, uploadedMap) {
  logProgress(`Post Upload Processing item ${++counter}: ${item.title}`);

  updateFeatureImage(item, uploadedMap);

  if (!item.err) {
    const name = path.basename(item.preProcessedHtml);
    const fileStr = await fs.readFile(item.preProcessedHtml, 'utf-8');
    const $dom = toDOM(fileStr);
    replaceUploadedFiles($dom, uploadedMap);
    const preparedPath = path.join(config.preparedHtmlDir, name);
    item.preparedHtml = preparedPath;
    await fs.outputFile(preparedPath, $dom('body').html());
  }
}

const generateJobs = (items, uploadedMap) => {
  let jobs = items.map(item => {
    const job = () => postUploadJob(item, uploadedMap);
    return job;
  });
  items.forEach(item => {
    if (item.children) {
      const childJobs = generateJobs(item.children, uploadedMap)
      jobs = [...jobs, ...childJobs];
    }
  });
  return jobs;
}


module.exports.postUploadProcess = async function() {
  logStep('Post Upload Processing...')
  const preStr = await fs.readFile(config.preProcessedMetaFile, 'utf-8');
  const {items} = JSON.parse(preStr);

  const uploadedStr = await fs.readFile(config.uploadedFilesPath, 'utf-8');
  const uploadedMap = JSON.parse(uploadedStr);

  const promises = generateJobs(items, uploadedMap.items);
  await PromisePool.for(promises)
    .withConcurrency(40)
    .process(job => job());

  console.log(); // new line
  const results = {items}
  await fs.outputFile(config.preparedMetaFile, JSON.stringify(results, null, 2))
  logStep('DONE - Post Upload Processing\n')
}

