const getFieldObject = (field) => {
  if (Array.isArray(field)) {
    return { type: 'string', xpaths: field };
  }
  return field;
}

const getBoolValue = (val) => {
  if (val === 'לא') {
    return false;
  }
  if (val === 'כן') {
    return true;
  }
  return null;
}
const getParsedValue = (val, type) => {
  switch (type) {
  case 'int':
    return parseInt(val);
  case 'bool':
    return getBoolValue(val);
  case 'string':
  default:
    return val;
  }
}
const scrapeFields = async (page, fields) => {
  const keys = Object.keys(fields);
  const result = {};
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const { xpaths, type } = getFieldObject(fields[key]);
    const value = await helpers.tryGetContent(page, xpaths);

    result[key] = getParsedValue(value, type);
  }
  return result;
}


const baseUrl = 'https://projects.jerusalemfoundation.org/education.aspx';


const getProject = async (page, link) => {
  try {
    await page.goto(link, { timeout: 0 });
    const liRes = {
      title: await page.$eval('.project-name', el => el.textContent.trim()),
      year: await page.$eval('.projectYear', el => el.textContent.trim()),
      address: await page.$eval('.address', el => el.textContent.trim()),
      content: await page.$eval('#about .text', el => el.textContent.trim()),
      images: await page.$$eval('#galleryAccordion img', imgs => (imgs || []).map(i => i.src)),
      url: link,
    }
    const donorsLink = await page.$('.menu-links .donors a');
    if (donorsLink) {
      await donorsLink.click();
      await page.waitForSelector('#donorsCarousel a');
      const donors = await page.$$eval('#donorsCarousel a', els => els.map(el => el.href));
      liRes.donors = donors;
    }
    const [relatedLink] = await page.$x('//li[@class="bg2"]/a[text()="Related"]')
    if (relatedLink) {
      await relatedLink.click();
      await page.waitForSelector('#ProjectRelatedProjects_hidden_ul a');
      const relatedProjects = await page.$$eval('#ProjectRelatedProjects_hidden_ul a', els => els.map(el => el.href));
      liRes.relatedProjects = relatedProjects;
    }
    return liRes;
  } catch (err) {
    console.error(`Failed to process ${link}`);
    return { url: link, err };
  }

}

const processDomain = async (domain, page, linksPage) => {
  await page.goto(domain.url, { timeout: 0 });
  await page.waitForSelector('#ProjectsCarousel_hidden_ul li');
  const liHandles = await page.$$('#ProjectsCarousel_hidden_ul li')

  const domainResults = [];
  for (let i = 0; i < liHandles.length; i++) {
    const liHandle = liHandles[i];
    const link = await liHandle.$eval('a', a => a.href);
    console.log(`Processing ${i + 1}/${liHandles.length} in domain ${domain.text}`);
    const liRes = await getProject(linksPage, link);
    domainResults.push(liRes);
  }

  fs.writeFileSync(`${domain.text}.json`, JSON.stringify(domainResults, null, 4), 'utf8')
}

const getDonor = async (donorLink, page) => {
  try {
    await page.goto(donorLink, { timeout: 0 });
    const imgsFlagsUrls = await page.$$eval('.donor-top-box img', imgs => (imgs || []).map(i => i.src));
    const liRes = {
      title: await page.$eval('.donor-top-box h3', el => el.title.trim()),
      countries: imgsFlagsUrls.map(url => {
        if (flags[url]) { return flags[url] }
        throw new Error('no flag for ' + url)
      }),
      url: donorLink,
    }
    return liRes;
  } catch (err) {
    console.error(`Failed to process ${donorLink}: ${err}`);
    return { url: donorLink, err: err.message };
  }
}

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: null,
  });

  const domains = [{
    url: 'https://projects.jerusalemfoundation.org/economic-growth.aspx',
    text: 'ECONOMIC GROWTH',
  }, {
    url: 'https://projects.jerusalemfoundation.org/education.aspx',
    text: 'EDUCATION',
  }, {
    url: 'https://projects.jerusalemfoundation.org/vulnerable-populations.aspx',
    text: 'VULNERABLE POPULATIONS',
  }, {
    url: 'https://projects.jerusalemfoundation.org/dialogue.aspx',
    text: 'DIALOGUE',
  }, {
    url: 'https://projects.jerusalemfoundation.org/art-culture.aspx',
    text: 'Art & Culture',
  }, {
    url: 'https://projects.jerusalemfoundation.org/heritage.aspx',
    text: 'HERITAGE',
  }]

  const page = await browser.newPage();
  // const linksPage = await browser.newPage();

  const startTime = Date.now();
  console.log(`Start Time: ` + new Date(startTime));

  // for (let i = 0; i < domains.length; i++) {
  //     const domain = domains[i];
  //     await processDomain(domain, page, linksPage);
  // }

  let mySet = new Set();
  for (let i = 0; i < domains.length; i++) {
    const domain = domains[i];
    const str = fs.readFileSync(`${domain.text}.json`, 'utf8')
    const json = JSON.parse(str);
    json.forEach(i => {
      if (!i.donors) { return; }
      i.donors.forEach(donor => mySet.add(donor));
    })
  }
  const donorLinks = Array.from(mySet);
  fs.writeFileSync(`donorLinks.json`, JSON.stringify(donorLinks, null, 4), 'utf8')

  const donorsStr = fs.readFileSync(`donors.json`, 'utf8')
  const donors = JSON.parse(donorsStr);

  const d = {}
  for (let i = 0; i < donors.length; i++) {
    const donor = donors[i];
    if (d[donor.title]) {
      throw new Error(d.title);
    }
    d[donor.title] = true;
    console.log(donor);
    // const cachedIndex = donors.findIndex(d => d.url === donorLink);
    // if (cachedIndex === -1) {
    //     const donor = await getDonor(donorLink, page);
    //     donors.push(donor);
    // } else {
    //     const cached = donors[cachedIndex];
    //     if (!cached.err) {continue;}
    //     const donor = await getDonor(donorLink, page);
    //     donors[cachedIndex] = donor;
    // }
    // console.log(`processing donor ${i+1}/${donorLinks.length}`)
    // fs.writeFileSync(`donors.json`, JSON.stringify(donors, null, 4), 'utf8')
  }
  // console.log(mySet.size);

  // console.log(`End Time: ` + new Date(startTime));

})();
